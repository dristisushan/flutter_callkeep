//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<flutter_callkeep/CallKeepPlugin.h>)
#import <flutter_callkeep/CallKeepPlugin.h>
#else
@import flutter_callkeep;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [CallKeepPlugin registerWithRegistrar:[registry registrarForPlugin:@"CallKeepPlugin"]];
}

@end
